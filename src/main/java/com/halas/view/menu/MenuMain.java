package com.halas.view.menu;

import com.halas.controller.task1.pingPong.PingPong;
import com.halas.controller.task2.RunFibonacciWithThreads;
import com.halas.controller.task3.RunFibonacciWithExecutors;
import com.halas.controller.task4.RunTaskFour;
import com.halas.controller.task5.MyScheduledThreadPool;
import com.halas.controller.task6.RunTaskSix;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

import static com.halas.model.consts.ConstDescriptionException.CORRECT_POINT;
import static com.halas.model.consts.ConstDescriptionException.NULL_POINTER_EXCEPTION;
import static com.halas.model.consts.menu.Const.*;

public class MenuMain {
    private static final Logger LOG = LogManager.getLogger(MenuMain.class);
    private static final Scanner INPUT = new Scanner(System.in);

    private Map<String, String> menuDescriptions;
    private Map<String, Functional> menuMethods;

    private MenuMain() {
        initMenu();
    }

    private void initMenu() {
        menuDescriptions = new LinkedHashMap<>();
        menuMethods = new LinkedHashMap<>();

        menuDescriptions.put("1", POINT_ONE);
        menuDescriptions.put("2", POINT_TWO);
        menuDescriptions.put("3", POINT_THREE);
        menuDescriptions.put("4", POINT_FOUR);
        menuDescriptions.put("5", POINT_FIVE);
        menuDescriptions.put("6", POINT_SIX);
        menuDescriptions.put("7", POINT_SEVEN);
        menuDescriptions.put("0", POINT_EXIT);

        menuMethods.put("1", PingPong::run);
        menuMethods.put("2", RunFibonacciWithThreads::run);
        menuMethods.put("3", RunFibonacciWithExecutors::run);
        menuMethods.put("4", RunTaskFour::run);
        menuMethods.put("5", MyScheduledThreadPool::run);
        menuMethods.put("6", RunTaskSix::run);

        menuMethods.put("0", this::exitFromProgram);
    }

    private void showMenuDescriptions() {
        menuDescriptions.values().forEach(LOG::info);
    }

    private void exitFromProgram(){
        System.exit(0);
    }

    public static void runMenuMain() {
        String keyForRunMenu;
        MenuMain menuMain = new MenuMain();
        while (true) {
            try {
                menuMain.showMenuDescriptions();
                LOG.info("Please select menu point.");
                keyForRunMenu = INPUT.nextLine();
                menuMain.menuMethods.get(keyForRunMenu).run();
            } catch (NullPointerException e) {
                LOG.error(CORRECT_POINT);
                LOG.error(NULL_POINTER_EXCEPTION);
            }
        }
    }
}