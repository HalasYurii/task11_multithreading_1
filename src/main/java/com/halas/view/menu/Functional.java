package com.halas.view.menu;

@FunctionalInterface
public interface Functional {
    void run();
}