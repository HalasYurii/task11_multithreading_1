package com.halas.model.consts;

public class ConstPrimitives {
    //for task1
    public static final int SIZE_OF_CYCLE = 100;
    public static final int HUNDRED = 100;

    //for task2
    public static final int CONVERT_NANO_TO_SEC = 1_000_000_000;
    public static final int COUNT_FIRST_FIBONACCI_DIGIT = 32;
    public static final int AMOUNT_THREADS = 4;

    //for task3
    public static final int FOUR_THOUSAND = 4000;

    //for task5
    public static final int ONE_THOUSAND = 1000;
    public static final int TEN = 10;

    //for task6
    public static final int ALL_ELEMENTS_VALUE = 10;
    public static final int ALL_ELEMENTS_DECREASE = 2;
    public static final int ALL_ELEMENTS_INCREASE = 2;
    public static final int SIZE_LIST_NUMBERS = 10;

    private ConstPrimitives() {
    }
}
