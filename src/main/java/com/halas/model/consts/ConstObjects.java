package com.halas.model.consts;

public class ConstObjects {
    //for task1
    public static final Object MONITOR_FOR_SYNC = new Object();
    public static final String PING_STRING = "Ping";
    public static final String PONG_STRING = "Pong";

    private ConstObjects() {
    }
}
