package com.halas.model.consts.menu;

public class Const {
    public static final String POINT_ONE = " 1 - (task 1)Ping-pong.";
    public static final String POINT_TWO = " 2 - (task 2)Fibonacci with threads.";
    public static final String POINT_THREE = " 3 - (task 3)Fibonacci with executors.";
    public static final String POINT_FOUR = " 4 - (task 4)Fibonacci, get sums the values.";
    public static final String POINT_FIVE = " 5 - (task 5)ScheduledThreadPool.";
    public static final String POINT_SIX = " 6 - (task 6)Task with critical section.";
    public static final String POINT_SEVEN = " 7 - (task 7).";
    public static final String POINT_EXIT = " 0 - Exit.";

    private Const() {
    }
}
