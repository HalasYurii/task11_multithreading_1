package com.halas.controller.task3;

import com.halas.controller.task3.Executors.MyExecutorService;
import com.halas.controller.task3.Executors.MyScheduledExecutorService;

import static com.halas.model.consts.ConstPrimitives.*;

public class RunFibonacciWithExecutors {

    public static void run() {
        MyExecutorService executorService = new MyExecutorService();
        executorService.runExecutorService(AMOUNT_THREADS);

        MyScheduledExecutorService scheduledExecutor = new MyScheduledExecutorService();
        scheduledExecutor.runWithScheduledExecutorService();
    }
}