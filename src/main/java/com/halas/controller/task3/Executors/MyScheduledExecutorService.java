package com.halas.controller.task3.Executors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.halas.controller.fibonacci.Fibonacci.getFibonacciDigitByIndex;
import static com.halas.model.consts.ConstDescriptionException.INTERRUPTED_EXCEPTION;
import static com.halas.model.consts.ConstPrimitives.COUNT_FIRST_FIBONACCI_DIGIT;
import static com.halas.model.consts.ConstPrimitives.FOUR_THOUSAND;

public class MyScheduledExecutorService {
    private static final Logger LOG = LogManager.getLogger(MyScheduledExecutorService.class);

    private Runnable createRunnableWithFibonacci() {
        return () -> {
            for (int i = 1; i <= COUNT_FIRST_FIBONACCI_DIGIT; i++) {
                LOG.info(getFibonacciDigitByIndex(i));
            }
        };
    }

    public void runWithScheduledExecutorService() {
        try {
            LOG.debug("runWithScheduledExecutorService");
            ScheduledExecutorService singleExecutor =
                    Executors.newSingleThreadScheduledExecutor();
            Runnable task = createRunnableWithFibonacci();
            int initialDelay = 0;
            int period = 2;
            singleExecutor.scheduleWithFixedDelay(task, initialDelay, period, TimeUnit.SECONDS);
            Thread.sleep(FOUR_THOUSAND);
            singleExecutor.shutdownNow();
        } catch (InterruptedException e) {
            LOG.error(INTERRUPTED_EXCEPTION);
        }
    }
}