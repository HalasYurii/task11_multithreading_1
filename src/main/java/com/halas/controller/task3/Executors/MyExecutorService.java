package com.halas.controller.task3.Executors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static com.halas.controller.fibonacci.Fibonacci.getFibonacciDigitByIndex;
import static com.halas.model.consts.ConstDescriptionException.EXECUTION_EXCEPTION;
import static com.halas.model.consts.ConstDescriptionException.INTERRUPTED_EXCEPTION;
import static com.halas.model.consts.ConstPrimitives.*;

public class MyExecutorService {
    private static final Logger LOG = LogManager.getLogger(MyExecutorService.class);

    private Future<Object> createFutureObjectByService(ExecutorService service) {
        return service.submit(() -> {
            for (int i = 1; i <= COUNT_FIRST_FIBONACCI_DIGIT; i++) {
                LOG.info(getFibonacciDigitByIndex(i));
            }
            return new Object();
        });
    }

    /**
     * First 42 fibonacci digits:
     * Time with 4 threads: 26.39 sec.
     * Time with 2 threads: 25.23 sec.
     * Time with 1 thread: 28.27 sec.
     */
    public void runExecutorService(int numberOfThreads) {
        try {
            LOG.debug("runExecutorService");
            long startTime = System.nanoTime();
            ExecutorService service =
                    Executors.newFixedThreadPool(numberOfThreads);
            Future<Object> future = createFutureObjectByService(service);
            future.get();
            long endTime = System.nanoTime();
            double executionTime = (endTime - startTime * 1.) / CONVERT_NANO_TO_SEC;
            LOG.debug("Execution time: " + executionTime + "\n\n");
        } catch (InterruptedException e) {
            LOG.error(INTERRUPTED_EXCEPTION);
        } catch (ExecutionException e) {
            LOG.error(EXECUTION_EXCEPTION);
        }
    }
}
