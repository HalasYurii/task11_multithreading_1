package com.halas.controller.task1.pingPong;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.halas.model.consts.ConstObjects.*;
import static com.halas.model.consts.ConstPrimitives.*;
import static com.halas.model.consts.ConstDescriptionException.INTERRUPTED_EXCEPTION;

public class PingPong {
    private static final Logger LOG = LogManager.getLogger(System.in);
    //змінна щоб контролювати що почався саме перший поток
    private volatile boolean isFirstStart;

    private PingPong() {
    }

    private Thread getWithFirstWait(String outputMessage) {
        return new Thread(() -> {
            synchronized (MONITOR_FOR_SYNC) {
                for (int i = 0; i < SIZE_OF_CYCLE; i++) {
                    try {
                        isFirstStart = true;
                        MONITOR_FOR_SYNC.wait();
                    } catch (InterruptedException ignored) {
                    }
                    LOG.info(outputMessage);
                    MONITOR_FOR_SYNC.notify();
                }
            }
        });
    }

    private Thread getWithFirstNotify(String outputMessage) {
        return new Thread(() -> {
            synchronized (MONITOR_FOR_SYNC) {
                for (int i = 0; i < SIZE_OF_CYCLE; i++) {
                    MONITOR_FOR_SYNC.notify();
                    LOG.info(outputMessage);
                    try {
                        MONITOR_FOR_SYNC.wait();
                    } catch (InterruptedException ignored) {
                    }
                }
            }
        });
    }

    /**
     * Для коректної роботи програми повинен початись перший потік
     * і лише після того другий, бо інакше можливе вічне чекання
     * це дуже важлива перевірка.
     */
    private void pauseIfFirstNotBegun() {
        if (!isFirstStart) {
            try {
                Thread.sleep(HUNDRED);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void startNewThread(String firstOutputMessage,
                                String secondOutputMessage) {

        this.isFirstStart = false;
        Thread first = getWithFirstWait(firstOutputMessage);
        Thread second = getWithFirstNotify(secondOutputMessage);
        first.start();
        pauseIfFirstNotBegun();
        second.start();
        try {
            first.join();
            second.join();
        } catch (InterruptedException e) {
            LOG.error(INTERRUPTED_EXCEPTION);
        }
    }

    public static void run() {
        PingPong pingPong = new PingPong();
        pingPong.startNewThread(PING_STRING,PONG_STRING);
    }
}