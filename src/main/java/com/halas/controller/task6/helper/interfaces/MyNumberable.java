package com.halas.controller.task6.helper.interfaces;

public interface MyNumberable {

    void setAllElementsValue();

    void decreaseAllElements();

    void increaseAllElements();

    void showListNumbers();
}
