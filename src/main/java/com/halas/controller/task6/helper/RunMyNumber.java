package com.halas.controller.task6.helper;

import com.halas.controller.task6.helper.interfaces.MyNumberable;
import com.halas.controller.task6.helper.synchronizedNo.MyNumberNotSync;
import com.halas.controller.task6.helper.synchronizedYes.MyNumberSync;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.halas.model.consts.ConstDescriptionException.INTERRUPTED_EXCEPTION;

public class RunMyNumber {
    private static final Logger LOG = LogManager.getLogger(RunMyNumber.class);

    public static MyNumberable getIsSync(boolean isSynchronized){
        MyNumberable task;
        if (isSynchronized) {
            task = new MyNumberSync();
        } else {
            task = new MyNumberNotSync();
        }
        return task;
    }

    public static void runMyNumber(boolean isSynchronized) {
        MyNumberable task;
        task = getIsSync(isSynchronized);
        Thread first = new Thread(task::setAllElementsValue);
        Thread second = new Thread(task::decreaseAllElements);
        Thread third = new Thread(task::increaseAllElements);
        first.start();
        second.start();
        third.start();
        try {
            first.join();
            second.join();
            third.join();
        } catch (InterruptedException e) {
            LOG.error(INTERRUPTED_EXCEPTION);
        }
        task.showListNumbers();
    }
}
