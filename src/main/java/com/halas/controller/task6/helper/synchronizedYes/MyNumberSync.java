package com.halas.controller.task6.helper.synchronizedYes;

import com.halas.controller.task6.helper.interfaces.MyNumberable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

import static com.halas.model.consts.ConstObjects.MONITOR_FOR_SYNC;
import static com.halas.model.consts.ConstPrimitives.*;

public class MyNumberSync implements MyNumberable {
    private static final Logger LOG = LogManager.getLogger(MyNumberSync.class);

    private List<Integer> listNumbers;

    public MyNumberSync() {
        initList();
    }

    private void initList() {
        listNumbers = new ArrayList<>();
    }

    @Override
    public synchronized void setAllElementsValue() {
        while (listNumbers.size() != SIZE_LIST_NUMBERS) {
            listNumbers.add(ALL_ELEMENTS_VALUE);
        }
    }

    @Override
    public synchronized void decreaseAllElements() {
        listNumbers.forEach(a ->
                listNumbers.set(listNumbers.lastIndexOf(a),
                        a -= ALL_ELEMENTS_DECREASE));
    }

    @Override
    public synchronized void increaseAllElements() {
            listNumbers.forEach(a ->
                    listNumbers.set(listNumbers.indexOf(a),
                            a -= ALL_ELEMENTS_INCREASE));
    }

    @Override
    public void showListNumbers() {
        LOG.debug("\n\nSynchronized list, all elements must be 10!");
        listNumbers.forEach(LOG::debug);
    }
}
