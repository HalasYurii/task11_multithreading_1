package com.halas.controller.task6;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.halas.controller.task6.helper.RunMyNumber.runMyNumber;

public class RunTaskSix {
    private static final Logger LOG = LogManager.getLogger(RunTaskSix.class);

    public static void run() {
        //run with Not Synchronized methods
        runMyNumber(false);

        //run with Synchronized methods
        //need update this methods. with
        //parameter true!!!!
        runMyNumber(true);
    }
}
