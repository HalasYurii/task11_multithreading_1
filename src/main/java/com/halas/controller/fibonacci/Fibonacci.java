package com.halas.controller.fibonacci;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Fibonacci {

    private Fibonacci() {
    }

    /**
     * function will give you
     * true if this number the fibonacci or false if not.
     *
     * @param someValue integer digit that will be checked
     * @return boolean type false or true about digit
     */
    private static boolean isFibonacci(final int someValue) {
        int first = 1;
        int second = 0;
        for (int i = 0; i < someValue; i++) {
            first += second;
            second = first - second;

            if (first > someValue) {
                return false;
            } else if (first == someValue) {
                return true;
            }
        }
        return false;
    }

    /**
     * function will set all odd fibonacci digits from some interval from
     * start to end interval and save the result in List
     * in instance list.
     *
     * @param countOfFirstDigitsFibonacci is amount of first fibonacci digits
     *                                    that you want get
     */
    public static List<Integer> getListOfFirstFibonacciNumbers(
            final int countOfFirstDigitsFibonacci) {
        List<Integer> listFibonacciDigits = new LinkedList<>();
        for (int i = 0, amount = 1; amount <= countOfFirstDigitsFibonacci; i++) {
            if (isFibonacci(i)) {
                listFibonacciDigits.add(i);
                amount++;
            }
        }
        return listFibonacciDigits;
    }

    public static Integer getFibonacciDigitByIndex(int index) {
        if (index <= 0) {
            return 0;
        }
        for (int i = 0, amount = 0; ; i++) {
            if (isFibonacci(i)) {
                amount++;
                if (amount == index) {
                    return i;
                }
            }
        }
    }

    private static boolean isNotCorrect(final int first, final int second) {
        return (first > second) || (first < 0);
    }

    /**
     * @param from means that it's first index of fibonacci
     *             digits that will be included
     * @param to   means that it's last index of fibonacci
     *             digits that will not be included
     */
    public static List<Integer> getListFibonacciNumbersFromToEnd(
            final int from,
            final int to) {

        if (isNotCorrect(from, to)) {
            return new ArrayList<>();
        }
        List<Integer> listFibonacciDigits = new LinkedList<>();
        for (int i = from; i < to; i++) {
            listFibonacciDigits.add(getFibonacciDigitByIndex(i));
        }
        return listFibonacciDigits;
    }
}