package com.halas.controller.task5;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.halas.model.consts.ConstDescriptionException.ILLEGAL_ARGUMENT_EXCEPTION;
import static com.halas.model.consts.ConstDescriptionException.INTERRUPTED_EXCEPTION;
import static com.halas.model.consts.ConstPrimitives.*;

public class MyScheduledThreadPool {
    private static final Logger LOG = LogManager.getLogger(MyScheduledThreadPool.class);
    private static final Scanner INPUT = new Scanner(System.in);

    private static int generateRandomTime(int from, int to) {
        Random random = new Random();
        return from + random.nextInt(to - from + 1);
    }

    private static void sleepAndDisplayTime() {
        try {
            LOG.debug("runExecutorService");
            int sleepTime = generateRandomTime(1, TEN) * ONE_THOUSAND;
            long startTime = System.nanoTime();
            Thread.sleep(sleepTime);
            long endTime = System.nanoTime();
            double executionTime = (endTime - startTime * 1.) / CONVERT_NANO_TO_SEC;
            LOG.debug("Expected time: " + 1. * sleepTime / ONE_THOUSAND);
            LOG.debug("Time that wait: " + executionTime + "\n");
        } catch (InterruptedException e) {
            LOG.error(INTERRUPTED_EXCEPTION);
        }
    }

    private static int inputCountOfExecution() {
        int count;
        while (true) {
            LOG.info("Write count of ScheduledThreadPool execution:");
            String inputInt = INPUT.nextLine();
            try {
                count = Integer.parseInt(inputInt);
                if (count <= 0) {
                    if (LOG.isTraceEnabled()) {
                        throw new IllegalArgumentException();
                    }
                }
                break;
            } catch (IllegalArgumentException e) {
                LOG.error(ILLEGAL_ARGUMENT_EXCEPTION);
            }
        }
        return count;
    }

    public static void run() {
        try {
            ScheduledExecutorService pool =
                    Executors.newSingleThreadScheduledExecutor();
            int size = inputCountOfExecution();
            int delayBeforeStart = 1;
            for (int i = 0; i < size; i++) {
                pool.schedule(
                        MyScheduledThreadPool::sleepAndDisplayTime,
                        delayBeforeStart,
                        TimeUnit.SECONDS);
            }
            pool.shutdown();
            while (!pool.isTerminated()) {
                Thread.sleep(ONE_THOUSAND);
            }
        } catch (InterruptedException e) {
            LOG.error(INTERRUPTED_EXCEPTION);
        }
    }
}