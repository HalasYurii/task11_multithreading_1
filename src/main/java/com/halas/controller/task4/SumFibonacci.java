package com.halas.controller.task4;

import java.util.concurrent.Callable;

import static com.halas.controller.fibonacci.Fibonacci.getListOfFirstFibonacciNumbers;

class SumFibonacci {

    Integer getSumOfFirstFibonacciDigits(
            int amountFirstFibonacciNumbers) {
        return getListOfFirstFibonacciNumbers(amountFirstFibonacciNumbers)
                .stream()
                .mapToInt(a -> a)
                .sum();
    }

    Callable<Integer> getSumOfFirstFibWithCallable(
            int amountFirstFibDigits) {
        return () -> getSumOfFirstFibonacciDigits(amountFirstFibDigits);
    }
}
