package com.halas.controller.task4;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.Callable;

import static com.halas.model.consts.ConstDescriptionException.EXCEPTION;
import static com.halas.model.consts.ConstPrimitives.COUNT_FIRST_FIBONACCI_DIGIT;

public class RunTaskFour {
    private static final Logger LOG = LogManager.getLogger(RunTaskFour.class);

    public static void run() {
        try {
            SumFibonacci sum = new SumFibonacci();
            int sumWithoutCallable =
                    sum.getSumOfFirstFibonacciDigits(COUNT_FIRST_FIBONACCI_DIGIT);
            Callable<Integer> sumWithCallable =
                    sum.getSumOfFirstFibWithCallable(COUNT_FIRST_FIBONACCI_DIGIT);

            LOG.debug("Sum with callable: " + sumWithCallable.call());
            LOG.debug("Sum without callable: " + sumWithoutCallable+"\n\n");
        } catch (Exception e) {
            LOG.info(EXCEPTION);
        }
    }
}